import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyTokenExpireComponent } from './verify-token-expire.component';

describe('VerifyTokenExpireComponent', () => {
  let component: VerifyTokenExpireComponent;
  let fixture: ComponentFixture<VerifyTokenExpireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyTokenExpireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyTokenExpireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
