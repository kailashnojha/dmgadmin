import { Component, OnInit } from '@angular/core';
import { PeopleApi, MeasuresApi, LoopBackAuth,LoopBackConfig} from './../../sdk/index';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})

export class UserDetailComponent implements OnInit {
  params:any;
  data:any = {};
  baseUrlStr:string;
  startDate:string="";
  endDate:string="";
  isEmpty:boolean=false;
  measure: any = {};
  errSuccMeasure = {
    isError:false,
    isSuccess:false,
    succMsg:"",
    errMsg:""
  }
  isViewMode:boolean = true;
  constructor(private location: Location, private measuresApi:MeasuresApi, private peopleApi:PeopleApi,private router : Router,private route: ActivatedRoute) { 

  }

  ngOnInit() {
	  this.route.params.subscribe( params => {
  		this.params = params;
  		console.log(this.params)
  		this.baseUrlStr = LoopBackConfig.getPath();
  		console.log(this.baseUrlStr)
  	});

  	this.startDateInit();
  	this.endDateInit();

  	this.getUserDetails();
  }

  openModal(data){
    this.isViewMode = false;
    this.measure = Object.assign({},data);
    $('#measureModal').modal("show");
    this.errSuccMeasure = {
      isError:false,
      isSuccess:false,
      succMsg:"",
      errMsg:""
    }
  }

  back(){
    this.location.back();
  }

  viewModal(data){
    this.isViewMode = true;
    this.measure = Object.assign({},data);
    $('#measureModal').modal("show");
     
  }


  editMeasure(editMeasureForm){
    
    if(editMeasureForm.valid){ 
      let $btn = $('#measureBtn');
      $btn.button('loading');
      this.errSuccMeasure = {isError:false,isSuccess:false,succMsg:"",errMsg:""};    

      this.measuresApi.editMeasurement(this.measure).subscribe((success)=>{
        editMeasureForm._submitted = false;
        $btn.button('reset');
        $("#measureModal").modal("hide");
        this.getUserDetails();
      },(error)=>{
        $btn.button('reset');
        console.log(error)
        this.errSuccMeasure = {isError:true,isSuccess:false,succMsg:"",errMsg:error.message};
      })
    }  
  }


  startDateInit(){
  	var _self = this;
  	setTimeout(()=>{
	  	var date_input = $('#startDate');
	  	date_input.datepicker({
	        format: 'mm/dd/yyyy',
	        todayHighlight: true,
	        autoclose: true,
	        clearBtn:true,
	        numberOfMonths: 1,
	        defaultDate: "+1d",
	        
	    }).on('changeDate',function(e){
	      	_self.startDate = e.date;
	    });	
    },0)
  	
  }

  endDateInit(){
  	var _self = this;
  	setTimeout(()=>{
	  	var date_input = $('#endDate');
	  	date_input.datepicker({
	        format: 'mm/dd/yyyy',
	        todayHighlight: true,
	        autoclose: true,
	        clearBtn:true,
	        numberOfMonths: 1,
	        defaultDate: "+1d",
	        
	    }).on('changeDate',function(e){
	      	_self.endDate = e.date;
	    });	
    },0)
  	
  }

  getUserDetails(){
    this.isEmpty = false;
  	this.peopleApi.getUserDetail(this.params.userId).subscribe((success)=>{
  		this.data = success.success.data;
  		this.isEmpty = true;
  	},(error)=>{
      this.isEmpty = true;
  	})
  }

}
