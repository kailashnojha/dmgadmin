import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetTokenExpireComponent } from './reset-token-expire.component';

describe('ResetTokenExpireComponent', () => {
  let component: ResetTokenExpireComponent;
  let fixture: ComponentFixture<ResetTokenExpireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetTokenExpireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetTokenExpireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
