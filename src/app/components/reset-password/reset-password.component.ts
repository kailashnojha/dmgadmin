import { Component, OnInit } from '@angular/core';
import { PeopleApi ,LoopBackAuth,LoopBackConfig} from './../../sdk/index';
import {ActivatedRoute, Router} from "@angular/router";
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
 
  obj:any = {};
  params:any;
  actionError:any = {
    isError:false,
    msg:""
  }

  actionSuccess:any = {
  	isSuccess : false,
  	msg:""
  }

  constructor(private peopleApi:PeopleApi,private router : Router,private route: ActivatedRoute) { }

  ngOnInit() {
   	this.route.params.subscribe( params => {
  		this.params = params;
  	});

  	$.backstretch("assets/img/login-bg.jpg", { speed: 500 });
  }

  public customHeader(header:Headers){
    header.delete("Authorization");
  	header.append("Authorization",LoopBackConfig.getAuthPrefix()+this.params.token);
   	return header;
  }

  reset(resetForm){
  	if(resetForm.valid){
  		if(this.obj.password == this.obj.conPassword){
			this.actionError.msg = "";
        	this.actionError.isError = false;
        	this.actionSuccess.isSuccess = false;
        	this.actionSuccess.msg="";
  			let btn = $("#resetBtn");
  			btn.button('loading'); 	
  			this.peopleApi.setPassword(
				this.obj.password,
				this.customHeader.bind(this)
			).subscribe((success)=>{
				console.log(success);
				btn.button('reset');
				this.actionSuccess.isSuccess = true;
        		this.actionSuccess.msg="Password reset successfully";
			},(error)=>{
				console.log(error)
				this.actionError.isError = true;
				btn.button('reset');
				if(error.statusCode == 401){
					this.actionError.msg = "Please again request for reset password";
        		}else{
        			this.actionError.msg = error.message;
        		}
				
				
			})
  		}
  	}
  }

}
