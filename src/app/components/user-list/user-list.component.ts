import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PeopleApi ,LoopBackAuth} from './../../sdk/index';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
 
  users:Array<any>=[];
  selectedUser:any={};
  action:string;
  isDateError:boolean= false;
  search:string = "";
  today:Date= new Date();
  actionError:any = {
    isError:false,
    msg:""
  }
  errSuccUser:any = {isError:false,isSuccess:false,succMsg:"",errMsg:""};
  filter:any = {};
  status:string = "";
  queryParams: any = {};
  constructor(private route:ActivatedRoute,private router:Router ,private ref:ChangeDetectorRef,private peopleApi:PeopleApi) {
    this.route.queryParamMap.subscribe((obj:any)=>{
      this.search = obj.params.search || "";
      this.queryParams.search = obj.params.search; 
      this.status = obj.params.status || "";
    })
     // console.log("is page referesh again");
  }

  ngOnInit() {
    // console.log("is page init again");
    let _self = this;
  	$(document).ready(function() {
      var date_input = $('input[name="date"]'); //our date input has the name "date"
      var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
      date_input.datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        numberOfMonths: 1,
        defaultDate: "+1d",
        startDate: '+0d',
        // endDate: '+30d'
      }).on('changeDate',function(e){
          _self.isDateError = false;
          // _self.ref.detectChanges();
      });
    })
    this.getUsers();
  }

  updateSearchStr(){
    setTimeout(()=>{
      this.queryParams.search = this.search == "" ? undefined : this.search;
      this.router.navigate([],{queryParams:this.queryParams});  
    },0);
    
  }

  getUsers(){
    this.getFilter();
    this.peopleApi.getAllUsers(this.filter).subscribe((success)=>{
      this.users = success.success;
    },(error)=>{

    })
  }

  statusChanged(){
    setTimeout(()=>{
      this.getUsers();
    },0)
  }

  openModel(user,action){
    this.actionError.msg = "";
    this.actionError.isError = false;
    this.isDateError = false;
    $('input[name="date"]')[0].value = "";
    this.action = action;
    this.selectedUser = user;
    $("#myModal").modal("show")
  }

  modalAction(){
    if(this.action == "approved")
      this.verifyUser(); 
    else
      this.blockUser();
  }


  checkValidityOrBlock(user){
    if(!user.adminVerified){
      if(!user.validityDate){
        return "pending";
      }else{
        return "block";
      } 
    }else{
      if(new Date(user.validityDate)>this.today){
        return "active";
      }else{
        return "expired"
      }
    }
  }

  getFilter(){
    this.queryParams.status = this.status;
    if(this.status == "Pending"){
      this.filter = {
        where:{
          adminVerified:false,
          validityDate:{exists:false}
        }
      }
    }else if(this.status == "Blocked"){
      this.filter = {
        where:{
          adminVerified:false,
          validityDate:{exists:true},
        }
      }
    }else if(this.status == "Expired"){
      this.filter = {
        where:{
          adminVerified:true,
          validityDate:{lt:new Date()}
        }
      }
    }else if(this.status == "Approved"){
      this.filter = {
        where:{
          adminVerified:true,
          validityDate:{gt:new Date()}
        }
      }
    }else{
      this.filter = {};
      this.queryParams.status = undefined;
    }

    this.router.navigate([],{queryParams:this.queryParams});

  }



  verifyUser(){
    let validityDate =$('input[name="date"]')[0].value;
    if(validityDate == "" || validityDate == undefined){
      this.isDateError = true;
    }else{
      validityDate = new Date(validityDate);
      this.peopleApi.adminVerify(this.selectedUser.id,validityDate).subscribe((success)=>{
        this.getUsers();
        $("#myModal").modal("hide");
      },(error)=>{
        this.actionError.msg = error.message;
        this.actionError.isError = true;
      })
    }

     /* this.peopleApi.adminVerify().subscribe((success)=>{
          this.getUsers();
          $("#myModal").modal("hide");
      },(error)=>{
      })*/
  }

  blockUser(){
    this.peopleApi.blockByAdmin(this.selectedUser.id).subscribe(()=>{
      this.getUsers();
      $("#myModal").modal("hide");
    },(error)=>{
      this.actionError.msg = error.message;
      this.actionError.isError = true;
    })
  }

  openEditModel(user){
    this.selectedUser = {...user};
    $("#editUser").modal("show") 
  }


  editUser(editUserForm){
    // console.log(this.selectedUser);

    if(editUserForm.valid){ 
      let $btn = $('#editUserBtn');
      $btn.button('loading');
      this.errSuccUser = {isError:false,isSuccess:false,succMsg:"",errMsg:""};    

      this.peopleApi.editUser(
        this.selectedUser.id,
        this.selectedUser.fullName,
        this.selectedUser.mobile,
        this.selectedUser.companyName,
        this.selectedUser.officeName
      ).subscribe((success)=>{
        editUserForm._submitted = false;
        $btn.button('reset');
        $("#editUser").modal("hide");
        this.getUsers();
      },(error)=>{
        $btn.button('reset');
        this.errSuccUser = { isError:true,isSuccess:false,succMsg:"",errMsg:error.message };
      })
    }  

  }



}
