import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any, searchText?: any): any {
  	if(!searchText || typeof searchText!="string"){
  		searchText = ""
  		return items;
  	}
    return items.filter((item)=>item.fullName.toLowerCase().includes(searchText.toLowerCase()));
  }

}
