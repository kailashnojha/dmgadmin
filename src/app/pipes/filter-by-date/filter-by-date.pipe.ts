import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByDate'
})
export class FilterByDatePipe implements PipeTransform {

  transform(items: any, key?: any ,startDate?: any,endDate?:any): any {

  	// if((typeof startDate==="string" || startDate instanceof Date) && (typeof endDate==="string" || endDate instanceof Date)){
    if(startDate instanceof Date &&  endDate instanceof Date){
  		/*if(startDate === "" || endDate === "")
  			return items;
  		else{*/
  			// console.log("start Date = > ",startDate)
  			/*startDate = new Date(startDate);
  			endDate = new Date(endDate);*/
        /*console.log("endDate",endDate,"type => ",typeof endDate,"is instance of date => ",endDate instanceof Date)
  			console.log("start Hours = ",startDate.getHours()," Minutes = ",startDate.getMinutes());
        console.log("end Hours = ",endDate.getHours()," Minutes = ",endDate.getMinutes());*/

        endDate.setHours(23,59,59);
        // console.log("end Hours = ",endDate.getHours()," Minutes = ",endDate.getMinutes());

        return items.filter((item)=>{
  				let date = new Date(item[key]);
  				if(date instanceof Date){
  					if(date>startDate && date<endDate){
  						return true;
  					}else{
  						return false;
  					}
  				}else{
  					return false;
  				}
  			});
  		// }
  	}else{
  		return items;
  	}
   	// return 
  }

}
