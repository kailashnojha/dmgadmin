import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(items: any, orderBy?: any, type?: any,order?: any): any {
  	

    let sortedItems = items.sort((a: any, b: any) => {

    	if(type == "date"){
    		let dateA = new Date(a[orderBy]);
	        let dateB = new Date(b[orderBy]);

	        if(order)

	        if (dateA > dateB) {
	            return (order === "ASC") ? -1 : 1;
	        } else if (dateA < dateB) {
	            return (order === "ASC") ? 1 : -1;
	        } else {
	            return 0;
	        }	
    	}else{
    		let strA = a[orderBy];
	        let strB = b[orderBy];

	        if(order === "ASC"){
	        	return strA.localeCompare(strB);
	        }else{
	        	return strA.localeCompare(strB) * -1;
	        }

    		
    	}
        
    });

    return sortedItems;
  }

}
