/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { JSONSearchParams } from '../core/search.params';
import { ErrorHandler } from '../core/error.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Job } from '../../models/Job';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Job` model.
 */
@Injectable()
export class JobApi extends BaseLoopBackApi {

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, searchParams, errorHandler);
  }

  /**
   * Patch attributes for a model instance and persist it into the data source.
   *
   * @param {any} id Job id
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - An object of model property name/value pairs
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Job` object.)
   * </em>
   */
  public patchAttributes(id: any, data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `success` – `{string}` - 
   *
   *  - `weight` – `{number}` - 
   *
   *  - `source` – `{object}` - 
   *
   *  - `insurance` – `{boolean}` - 
   *
   *  - `valueOfLoad` – `{number}` - 
   *
   *  - `insPremium` – `{number}` - 
   *
   *  - `budget` – `{number}` - 
   *
   *  - `netPrice` – `{number}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
   

  public approveByAdmin(jobId: any = {}, jobApproveStatus: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/approveByAdmin";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        jobId: jobId,
        jobApproveStatus: jobApproveStatus
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }   

  public getJobsByAdmin(jobApproveStatus: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobsByAdmin";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof jobApproveStatus !== 'undefined' && jobApproveStatus !== null) _urlParams.jobApproveStatus = jobApproveStatus;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }   

  public createJob(req: any = {}, title: any, weight: any, source: any,
		destination: any, valueOfGoods: any, budget: any, netPrice: any, 
		typeOfVehicle: any, date: any,endDate : any, description: any, natureOfGoods: any,
		typeOfJob: any, selectedRoute: any, distance: any, duration: any,
		vehicleTonnage:any,vehicleBodyType:any,requiredType : any,customHeaders?: Function): Observable<any> {
		
		let _method: string = "POST";
		let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
		"/Jobs/createJob";
		let _routeParams: any = {};
		let _postBody: any = {
		  data: {
			title: title,
			weight: weight,
			source: source,
			destination: destination,
			valueOfGoods: valueOfGoods,
			budget: budget,
			netPrice: netPrice,
			typeOfVehicle: typeOfVehicle,
			date: date,
			endDate : date,
			description: description,
			natureOfGoods: natureOfGoods,
			typeOfJob: typeOfJob,
			selectedRoute: selectedRoute,
			distance: distance,
			duration: duration,
			vehicleTonnage : vehicleTonnage,
			vehicleBodyType : vehicleBodyType,
			requiredType : requiredType
		  }
		};
		let _urlParams: any = {};
		let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
		return result;
  }

  
  public getJobPricing(vehicleName: any, tonnage: any = {}, distanceInMeter: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobPricing";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof vehicleName !== 'undefined' && vehicleName !== null) _urlParams.vehicleName = vehicleName;
    if (typeof tonnage !== 'undefined' && tonnage !== null) _urlParams.tonnage = tonnage;
    if (typeof distanceInMeter !== 'undefined' && distanceInMeter !== null) _urlParams.distanceInMeter = distanceInMeter;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  
  public getJobList(realm: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobs";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  
  public applyForJob(req: any = {}, jobId : any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/JobApplies/applyForJob";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        jobId: jobId
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  
  public LoginWithFB(access_token: any, realm : any, firebaseToken: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/auth/facebook/token";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof access_token !== 'undefined' && access_token !== null) _urlParams.access_token = access_token;
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof firebaseToken !== 'undefined' && firebaseToken !== null) _urlParams.firebaseToken = firebaseToken;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  
  public getNearJobs(location: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getNearJobs";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof location !== 'undefined' && location !== null) _urlParams.location = location;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
 
 
  public getJobById(jobId: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobById";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof jobId !== 'undefined' && jobId !== null) _urlParams.jobId = jobId;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Job`.
   */
  public getModelName() {
    return "Job";
  }
}
