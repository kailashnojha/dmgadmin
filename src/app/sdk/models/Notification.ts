/* tslint:disable */

declare var Object: any;
export interface NotificationInterface {
  "id"?: any;
  "notificationCreatorId"?: any;
  "notificationForId"?: any;
  "peopleId"?: any;
  "jobId"?: any;
  notificationCreator?: any;
  notificationFor?: any;
  people?: any;
  job?: any;
}

export class Notification implements NotificationInterface {
  "id": any;
  "notificationCreatorId": any;
  "notificationForId": any;
  "peopleId": any;
  "jobId": any;
  notificationCreator: any;
  notificationFor: any;
  people: any;
  job: any;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      path: 'Notifications',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "notificationCreatorId": {
          name: 'notificationCreatorId',
          type: 'any'
        },
        "notificationForId": {
          name: 'notificationForId',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
        "jobId": {
          name: 'jobId',
          type: 'any'
        },
      },
      relations: {
        notificationCreator: {
          name: 'notificationCreator',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'notificationCreatorId',
          keyTo: 'id'
        },
        notificationFor: {
          name: 'notificationFor',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'notificationForId',
          keyTo: 'id'
        },
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
        job: {
          name: 'job',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'jobId',
          keyTo: 'id'
        },
      }
    }
  }
}
