/* tslint:disable */

declare var Object: any;
export interface MeasuresInterface {
  "measure"?: Array<any>;
  "id"?: number;
  "peopleId"?: number;
  people?: any;
}

export class Measures implements MeasuresInterface {
  "measure": Array<any>;
  "id": number;
  "peopleId": number;
  people: any;
  constructor(data?: MeasuresInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Measures`.
   */
  public static getModelName() {
    return "Measures";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Measures for dynamic purposes.
  **/
  public static factory(data: MeasuresInterface): Measures{
    return new Measures(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Measures',
      plural: 'Measures',
      path: 'Measures',
      idName: 'id',
      properties: {
        "measure": {
          name: 'measure',
          type: 'Array&lt;any&gt;'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'number'
        },
      },
      relations: {
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
      }
    }
  }
}
