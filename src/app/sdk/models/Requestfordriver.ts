/* tslint:disable */

declare var Object: any;
export interface RequestfordriverInterface {
  "status": boolean;
  "id"?: any;
  "ownerId"?: any;
  "vehicleId"?: any;
  "peopleId"?: any;
  owner?: any;
  vehicle?: any;
}

export class Requestfordriver implements RequestfordriverInterface {
  "status": boolean;
  "id": any;
  "ownerId": any;
  "vehicleId": any;
  "peopleId": any;
  owner: any;
  vehicle: any;
  constructor(data?: RequestfordriverInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Requestfordriver`.
   */
  public static getModelName() {
    return "Requestfordriver";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Requestfordriver for dynamic purposes.
  **/
  public static factory(data: RequestfordriverInterface): Requestfordriver{
    return new Requestfordriver(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Requestfordriver',
      plural: 'Requestfordrivers',
      path: 'Requestfordrivers',
      idName: 'id',
      properties: {
        "status": {
          name: 'status',
          type: 'boolean',
          default: false
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
        "vehicleId": {
          name: 'vehicleId',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
        vehicle: {
          name: 'vehicle',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'vehicleId',
          keyTo: 'id'
        },
      }
    }
  }
}
