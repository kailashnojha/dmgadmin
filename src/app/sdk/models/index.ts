/* tslint:disable */
export * from './Email';
export * from './People';
export * from './Transaction';
export * from './Vehicle';
export * from './Container';
export * from './VehicleType';
export * from './Job';
export * from './JobApply';
export * from './BaseModels';
export * from './FireLoopRef';
export * from './Requestfordriver';
export * from './Otp';
export * from './Rating';
export * from './Measures';