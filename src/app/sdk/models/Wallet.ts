/* tslint:disable */

declare var Object: any;
export interface WalletInterface {
  "amount"?: number;
  "reserveAmount"?: number;
  "id"?: any;
  "peopleId"?: any;
  people?: any;
}

export class Wallet implements WalletInterface {
  "amount": number;
  "reserveAmount": number;
  "id": any;
  "peopleId": any;
  people: any;
  constructor(data?: WalletInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Wallet`.
   */
  public static getModelName() {
    return "Wallet";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Wallet for dynamic purposes.
  **/
  public static factory(data: WalletInterface): Wallet{
    return new Wallet(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Wallet',
      plural: 'Wallets',
      path: 'Wallets',
      idName: 'id',
      properties: {
        "amount": {
          name: 'amount',
          type: 'number',
          default: 0
        },
        "reserveAmount": {
          name: 'reserveAmount',
          type: 'number',
          default: 0
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
      }
    }
  }
}
