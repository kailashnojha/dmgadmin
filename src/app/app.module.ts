import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule } from './routing/routing.module';
import { FormsModule } from '@angular/forms';
import { SDKBrowserModule } from './sdk/index';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ResetTokenExpireComponent } from './components/reset-token-expire/reset-token-expire.component';
import { EmailVerifiedComponent } from './components/email-verified/email-verified.component';
import { VerifyTokenExpireComponent } from './components/verify-token-expire/verify-token-expire.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FilterPipe } from './pipes/filter/filter.pipe';
import { OrderByPipe } from './pipes/order-by/order-by.pipe';
import { OrderByDatePipe } from './pipes/order-by-date/order-by-date.pipe';
import { FilterByDatePipe } from './pipes/filter-by-date/filter-by-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserListComponent,
    ResetPasswordComponent,
    ResetTokenExpireComponent,
    EmailVerifiedComponent,
    VerifyTokenExpireComponent,
    UserDetailComponent,
    FilterPipe,
    OrderByPipe,
    OrderByDatePipe,
    FilterByDatePipe,
    PrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    PdfViewerModule,
    RoutingModule,
    SDKBrowserModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
