import { Component ,OnInit, HostListener} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router'
import { LoopBackConfig, LoopBackAuth, PeopleApi } from './sdk/index';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
    title = 'app';
    isLoginPage:boolean= true;

  	currentUrl:string = "";
  	offset:number = 0;
  	isNeedScroll:boolean = false;
    @HostListener('window:scroll', ['$event']) 
    doSomething(event) {
      // console.debug("Scroll Event", document.body.scrollTop);
      // see András Szepesházi's comment below
      if(this.currentUrl == "/user-list"){
      	this.offset = window.pageYOffset;	
      	this.isNeedScroll = false;
      	console.log("Scroll Event", this.offset );
      }
      	
      
    }

    constructor(private peopleApi:PeopleApi,private router:Router){
		LoopBackConfig.setBaseURL('http://139.59.71.150:3005');
		// LoopBackConfig.setBaseURL('http://localhost:3005');
		LoopBackConfig.setApiVersion('api'); 
    }

	ngOnInit() {
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			
			this.currentUrl = evt.url;

			if(this.currentUrl == "/user-list"){
				this.isNeedScroll = true;
				setTimeout(()=>{
					window.scrollTo(0,this.offset);	
				},1000)
				
			}else{
				this.isNeedScroll = false;
			}
			if(evt.url == "/" || evt.url.startsWith("/reset-password") || evt.url.startsWith("/privacy-policy"))
				this.isLoginPage = true;
			else{
				$(".backstretch").remove();
				this.isLoginPage = false
			}
		})	
		
	}	

	logout(){
		this.peopleApi.logout().subscribe((success)=>{
			this.router.navigate(['/']);
		},(error)=>{
			if(error.statusCode == 401){
				this.router.navigate(['/']);
			}
		})
	}


}
