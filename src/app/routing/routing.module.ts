import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './../components/login/login.component';
import { UserListComponent } from './../components/user-list/user-list.component';
import { ResetPasswordComponent } from './../components/reset-password/reset-password.component';
import { ResetTokenExpireComponent } from './../components/reset-token-expire/reset-token-expire.component';
import { EmailVerifiedComponent } from './../components/email-verified/email-verified.component';
import { VerifyTokenExpireComponent } from './../components/verify-token-expire/verify-token-expire.component';
import { UserDetailComponent } from './../components/user-detail/user-detail.component';
import { PrivacyPolicyComponent } from './../components/privacy-policy/privacy-policy.component';

const routes: Routes = [
	{ path: '', redirectTo: '/', pathMatch: 'full' },
	{ path: '', component: LoginComponent },
	{ path: 'user-list', component: UserListComponent },
	{ path: 'reset-password/:token', component: ResetPasswordComponent },
	{ path: 'reset-token-expire', component: ResetTokenExpireComponent },
	{ path: 'verified', component: EmailVerifiedComponent },
	{ path: 'verify-token-expire', component: VerifyTokenExpireComponent },
	{ path: 'user-detail/:userId', component: UserDetailComponent },
	{ path: 'privacy-policy', component: PrivacyPolicyComponent }
]; 

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class RoutingModule { }
